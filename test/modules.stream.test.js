/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-unused-expressions */

const { expect } = require('chai');
const stream = require('../src/modules/stream');

describe('Stream module', function () {
  it('starts a torrent stream', async function () {
    this.timeout(3000);
    const torrent = await stream.add('91007AD4B39D1F6F7C0F1B2C575972181BBF6D12');
    expect(torrent).to.haveOwnProperty('torrent');
    expect(torrent).to.haveOwnProperty('server');
    expect(torrent).to.haveOwnProperty('expires');
    await stream.end('91007AD4B39D1F6F7C0F1B2C575972181BBF6D12');
  });

  it('throws an error on non-existant stream', async function () {
    try {
      await stream.ping('91007AD4B39D1F6F7C0F1B2C575972181BBF6D12');
      expect.fail('Should of thrown an error');
    } catch (error) {
      expect(error).to.be.an('Error');
    }
  });

  it('updates the expire time on a torrent stream', async function () {
    const torrent = await stream.add('91007AD4B39D1F6F7C0F1B2C575972181BBF6D12');
    const { expires } = torrent;
    await stream.ping('91007AD4B39D1F6F7C0F1B2C575972181BBF6D12');
    expect(expires).to.be.lessThan(torrent.expires);
  });

  it('ends a torrent stream', async function () {
    await stream.end('91007AD4B39D1F6F7C0F1B2C575972181BBF6D12');
  });
});
