const express = require('express');

const stream = require('../modules/stream');

const router = express.Router();

router.get('/:hash([A-Z0-9]{40})', async (request, response) => {
  const torrent = await stream.add(request.params.hash);
  response.json({
    port: torrent.server.address().port,
    files: torrent.torrent.files.map((file, index) => `/${index}/${file.name}`)
  });
});

router.get('/:hash([A-Z0-9]{40})/ping', async (request, response) => {
  try {
    await stream.ping(request.params.hash);
    response.sendStatus(204);
  } catch (error) {
    response.status(404).json({ message: error.message });
  }
});

router.get('/:hash([A-Z0-9]{40})/end', async (request, response) => {
  try {
    await stream.end(request.params.hash);
    response.sendStatus(204);
  } catch (error) {
    response.status(404).json({ message: error.message });
  }
});

module.exports = router;
