const WebTorrent = require('webtorrent');

let client = new WebTorrent({ maxConns: 300 });

client.on('error', () => {
  client = new WebTorrent({ maxConns: 300 });
});

const announce = [
  'udp://open.demonii.com:1337/announce',
  'udp://tracker.openbittorrent.com:80',
  'udp://tracker.coppersurfer.tk:6969',
  'udp://glotorrents.pw:6969/announce',
  'udp://tracker.opentrackr.org:1337/announce',
  'udp://torrent.gresille.org:80/announce',
  'udp://p4p.arenabg.com:1337',
  'udp://tracker.leechers-paradise.org:6969'
];

const streams = new Map();

process.on('beforeExit', () => {
  streams.forEach(stream => stream.server.close());

  client.destroy();
});

const endStream = hash => new Promise((resolve, reject) => {
  if (streams.has(hash)) {
    client.remove(hash, () => {
      streams.get(hash).server.close();
      streams.delete(hash);
      resolve();
    });
  } else {
    reject(new Error('Torrent does not exist!'));
  }
});

const addStream = hash => new Promise((resolve) => {
  if (streams.has(hash)) {
    resolve(streams.get(hash));
  } else {
    streams.set(hash, {});

    client.add(hash, { announce }, (torrent) => {
      streams.set(hash, {
        server: torrent.createServer(),
        torrent,
        expires: (Date.now() / 1000) + 120
      });

      streams.get(hash).server.listen();
      resolve(streams.get(hash));
    });
  }
});

const pingStream = hash => new Promise((resolve, reject) => {
  if (!streams.has(hash)) {
    reject(new Error('Torrent does not exist!'));
  } else {
    streams.get(hash).expires = Date.now() / 1000 + 120;
    resolve(true);
  }
});

// Remove old servers
setInterval(() => {
  streams.forEach((server, hash) => {
    if (streams.expires < (Date.now() / 1000)) {
      endStream(hash);
    }
  });
}, 180000);

module.exports = {
  end: endStream,
  add: addStream,
  ping: pingStream
};
