const http = require('http');
const app = require('./app');

if (!process.argv[2] || !parseInt(process.argv[2], 10)) {
  process.stderr.write('You must specify a port!');
  process.exit(1);
}

http
  .createServer(app)
  .listen(process.argv[2]);
