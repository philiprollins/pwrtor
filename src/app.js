const express = require('express');
const bodyParser = require('body-parser');

const streamRoutes = require('./routes/stream');

const app = express();

app.use((request, response, next) => {
  response.header('Access-Control-Allow-Origin', '*');
  response.header('Access-Control-Allow-Methods', 'GET');
  response.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(streamRoutes);

app.use((request, response, next) => {
  const error = new Error('Not Found!');
  error.status = 404;
  next(error);
});

/* eslint-disable no-unused-vars */
app.use((error, request, response, next) => {
  const message = process.env.NODE_ENV === 'development' ? error : {};
  response.statusCode = error.status || 500;
  response.json({
    status: 'error',
    message
  });
});
/* eslint-enable no-unused-vars */

module.exports = app;
